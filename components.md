# Robot components

## Fundamental components

| Component | Quantity | Description |
| ---      |  ---- | --- |
| Brushless Servomotor | 12 | Actuators of the robot |
| Disk 25T | 8 | Coupling for the hip1 and hip2 joints, 2 for each leg |
| Pulley GT2 36 teeth | 4  | Transmission for the knee joint, 1 for each leg |
| Servo heatsink | 2 | Heatsink for the servomotors in the knee joint |
| 7mm Bearing | 20  | 4 in each leg to tight the belts in the knee joint, 1 in each leg to support the knee joint |
| 37mm Bearing | 8 | 2 in each leg, to support the hip1 and hip2 joints |
| 3D printing threads | 16 | 4 in each leg, to fix the 7mm bearings that tight the belt |
| GT2 belt, 30cm long | 4 | 1 in each leg for the knee joint |
| White PLA 1kg | 1 |  To print the robot |
| Black PLA 1kg | 1 |  To print the robot |
| DC to DC converter 10A | 1 | To provide power to the servomotors |
| Teensy 4.0 | 1 |  To control the position of the servomotors and the interface with the IMU sensor |
| Relay 5V | 1 | To switch on and off the servomotors |
| 5V, 3.2A voltage regulator | 1 |  To provide power to the raspberry pi |
| Switch | 1 |  to turn on and off the robot |
| Rubber for the feet | 4 |  1 in each leg. This prevents the robot from sliding in the ground |
| Battery | 1 |  Provides power to all the electronics components |
| Raspberry pi 4 | 1 |  Main onboard computer |
| Cooling system raspberry pi | 1 | To cool down the raspberry pi |
| IMU BNO080 | 1 | To measure robot state |
| Digital Hall effect sensor | 4 | To sense the contact with the ground on the feet|


## Components for the face
| Component | Quantity | Description |
| ---      |  ---- | --- |
| Teensy 4.0 | 1 | To control the animation of the eyes in the displays |
| 128x64 SSH1106 OLED LCD Display | 2 | To animate the eyes of the robot |


## Components for navigation
| Component | Quantity | Description |
| ---      |  ---- | --- |
| YDLIDAR X2L | 1 | to obtain 2D distance measurements and build maps |
| Raspberry pi Camera 2.0 | 1 | For vision |

# Price and links
You can take a look to the [price list ](price_list.ods) and check out where to buy all these components. The price for all the components is around 1000 euros. Almost have of the price is due to the Brushless motors. If you want to try cheaper motors to reduce the price, go ahead, however it might not work as expected.

 Feel free to buy components from other sites, as the links provided here are just a guide.

