# PAVLOV MINI: Quadruped robot
Project in progress. Not fisnihed yet. I will document all the code through my youtube channel

![Demo](images/looking_around.gif)




## Description
Pavlov mini is a DIY 3D printed robot dog that uses hobby servomotors. It is inspired by MIT Cheetah and uses similar algorithms adapted to work with hobby servomotors. The control is embebed in ROS (Robot Operating System) and works under ROS noetic with a raspberry pi 4 running Ubuntu. The control software is written in C++, while some other packages in python are used to control/visualize the robot. 

# [Robot components and price](components.md)

# [Setup](setup.md)

# [3D printing](3D_printing.md)


